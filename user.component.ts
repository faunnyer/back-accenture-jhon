import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../../app/services/cliente.service';
import { Cliente } from '../model/cliente.class';

@Component({
    selector: 'user-cmp',
    moduleId: module.id,
    templateUrl: 'user.component.html'
})

export class UserComponent implements OnInit {

    clientes: any[] = [];


    cliente:Cliente = new Cliente();


    constructor(private _ClientesService: ClienteService) { }

    ngOnInit() {
        this._ClientesService.getClientes().subscribe(data => {
            this.clientes = data.object;
        }, error => {
            console.log(error);
        });
    }


    guardarCliente() {

        this._ClientesService.guardarCliente(this.cliente).subscribe(
            data => {
                console.log('registrado');
            }, err => {
                console.log(err);
            });
    }
}

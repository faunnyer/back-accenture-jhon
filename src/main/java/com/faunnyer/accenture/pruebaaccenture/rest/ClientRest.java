package com.faunnyer.accenture.pruebaaccenture.rest;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.faunnyer.accenture.pruebaaccenture.entity.Cliente;

@RepositoryRestResource(collectionResourceRel = "clientes", path = "clientes")
public interface ClientRest extends PagingAndSortingRepository<Cliente, Integer> {
	@RestResource(path = "identificacion", rel = "identificacion")
	public List<Cliente> findByIdentificacion(@Param("id") Integer identificacion);
}

package com.faunnyer.accenture.pruebaaccenture.rest;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.faunnyer.accenture.pruebaaccenture.entity.Cliente;
import com.faunnyer.accenture.pruebaaccenture.entity.Solicitud;

@RepositoryRestResource(collectionResourceRel = "solicitudes", path = "solicitudes")
public interface SolicitudRest extends PagingAndSortingRepository<Solicitud, Integer> {
//	@RestResource(path = "nameStartsWith", rel = "nameStartsWith")
//	public List<Cliente> findByNameStartsWith(@Param("nombre") String nombre);
}

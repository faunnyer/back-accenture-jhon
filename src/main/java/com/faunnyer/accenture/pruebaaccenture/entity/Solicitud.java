package com.faunnyer.accenture.pruebaaccenture.entity;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="SOLICITUDES")
public class Solicitud {
	
	@Id
	@Column(name="CDSOLICITUD")
	private Integer idSolicitud;
	
	@Column(name="NOMBRE_EMPRESA")
	private String nombreEmpresa;
	
	@Column(name="NIT")
	private Integer nit;
	
	@Column(name="SALARIO_ACTUAL")
	private Integer salarioActual;
	
	
	@Column(name="FECHA_INGRESO")
	private String fechaNacimientoIngreso;

	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CDCLIENTE")
	private Cliente cliente;

	public Integer getIdSolicitud() {
		return idSolicitud;
	}


	public void setIdSolicitud(Integer idSolicitud) {
		this.idSolicitud = idSolicitud;
	}


	public String getNombreEmpresa() {
		return nombreEmpresa;
	}


	public void setNombreEmpresa(String nombreEmpresa) {
		this.nombreEmpresa = nombreEmpresa;
	}


	public Integer getNit() {
		return nit;
	}


	public void setNit(Integer nit) {
		this.nit = nit;
	}


	public Integer getSalarioActual() {
		return salarioActual;
	}


	public void setSalarioActual(Integer salarioActual) {
		this.salarioActual = salarioActual;
	}


	public String getFechaNacimientoIngreso() {
		return fechaNacimientoIngreso;
	}


	public void setFechaNacimientoIngreso(String fechaNacimientoIngreso) {
		this.fechaNacimientoIngreso = fechaNacimientoIngreso;
	}


	public Cliente getCliente() {
		return cliente;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	

	
}

package com.faunnyer.accenture.pruebaaccenture;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class PruebaaccentureApplication {

	public static void main(String[] args) {
		try {
			/*Create Directory By default*/
			Files.createDirectories(Paths.get("db"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		SpringApplication.run(PruebaaccentureApplication.class, args);
	}
}
